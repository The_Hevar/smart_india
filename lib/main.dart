import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Phonebook',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Phonebook'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  List<Contact> contacts = [];             //this list is provide us our contact in listView.
  List<Contact> findContacts = [];         //this list use for comparing ,when we use our searchController.

  TextEditingController searching = new TextEditingController();  //This is our searchController variable.

  @override
  void initState(){
    super.initState();
    getAllContacts();
    searching.addListener(() {
      searchContact();
    });
  }
        // By the use of contact_service library we can get our mobile contact .We use there getAllContacts function for get contacts.
  getAllContacts() async{
    List<Contact> _contacts = (await ContactsService.getContacts(withThumbnails: false)).toList();   //for getting our mobile contacts.
    setState(() {
      contacts = _contacts;
    });
  }

  //This function is use for comparing the contact which we search on searchController and then show it to our phone screen
  searchContact(){
    List<Contact> _findContacts = [];
    _findContacts.addAll(contacts);
    if (searching.text.isNotEmpty){
      _findContacts.retainWhere((element) {                       //This function will provide a bool value
        String findWord = searching.text.toLowerCase();           // findWord variable use for getting the a contact by searchController
        String elementName = element.displayName.toLowerCase();   // elementName gives the contact which we want to compare to our searchController contact.

        bool stringMatch = elementName.contains(findWord);

        if (stringMatch == true){
          return true;
        }
        var phoneNumber = element.phones.firstWhere((number) {    // firstWhere also gives a bool value for getting our contact by phoneNumber.
          return number.value.contains(findWord);                 //number provide us to the contact number by searchController.
        },
        orElse: () => null);                                      //orElse is necessary but basically not much useful.

        return phoneNumber != null;

      });
      setState(() {
        findContacts = _findContacts;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    bool isSearching = searching.text.isNotEmpty;              //use for searchController,this will help us for defining the conditions.
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget> [
            Container(
              child: TextField(
                controller: searching,
                decoration: InputDecoration(
                  labelText: 'Finder',
                  border: new OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(28)),
                    borderSide: new BorderSide(
                      color: Theme.of(context).primaryColor,
                    )
                  ),
                  prefixIcon: Icon(
                    Icons.search,
                    color: Theme.of(context).primaryColor,       //use for searchIcon.
                  ),
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(                                                             // showing all contacts as list .
                shrinkWrap: true,
                itemCount: isSearching == true? findContacts.length :contacts.length,              // itemCount is use as a variable here .
                itemBuilder: (context , index){
                  Contact contact = isSearching == true ? findContacts[index] :contacts[index];    //(***)This condition use when we type some name in our Finder and if our findContacts list element match with contacts list than it will be true.
                  return ListTile(                                                                 //This will return our mobile contacts into our new new app as line by line.
                    title: Text(contact.displayName),
                    subtitle: Text(contact.phones.elementAt(0).value),
                    leading: (contact.avatar != null && contact.avatar.length >0)?
                        CircleAvatar(backgroundImage: MemoryImage(contact.avatar),)                 //This will give an image in our CircleAvatar. This property we will not use there because at there we will not work on thumbnails .
                        : CircleAvatar(child: Text(contact.initials())),                            //In this project we will use this output for showing String inFront of our contacts in CircleAvatar.
                  );
                },
              ),
            )
          ],
        )
      )
    );
  }
}


//Thank you for this task it's benn amazing work in this task. I hope you will admire my work.